#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0+

FROM registry.hub.docker.com/library/postgres:alpine

LABEL Maintainer="oliver@schinagl.nl"

COPY dockerfiles/buildenv_check.sh /test/buildenv_check.sh
